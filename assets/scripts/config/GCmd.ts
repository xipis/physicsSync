const GCmd = {
    ClientCmd: {
        USER_LOGIN: "user_login",
        BALL_MOVE: "ball_move",
        
    },
    ServerCmd: {
        USER_LOGIN_SUCCESS: "user_login_success",
        USER_LOGIN_FAIL: "user_login_fail",
        UPDATE_FRAME: "update_frame",
    },
}
export default GCmd;