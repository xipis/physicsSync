const GEvent = {
    USER_LOGIN_SUCCESS: "user_login_success",
    USER_LOGIN_FAIL: "user_login_fail",
    UPDATE_FRAME: "update_frame",
}
export default GEvent;